# StackOverflow users information

## Table of Contents

1. [ Description ](#desc)
2. [ General Install ](#setup)

## 1. Description
This project is used to retrieve data from StackOverflow api. The main task for this project was to get users’ info by different criteria - such as users' country (Moldova, Romania), level, programming language, points, tags etc. Based on the daily limit for StackOverflow there was taken a decision to connect via special query-api and a special library that makes this app retrieve the results in less than 5 minutes on different Threads.

## 2. General Setup
You will need to create a verified account on StackOverflow and insert your credentials inside application configuration. To change your query-api with different criteria you will need to create your special query.
