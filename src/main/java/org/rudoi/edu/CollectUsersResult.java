package org.rudoi.edu;

import org.apache.commons.io.IOUtils;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;

import java.io.*;
import java.util.*;


public class CollectUsersResult {

    private static final String EMAIL = "your@email.com", PASSWORD = "";
    private static final String FILE_NAME = "users-list.txt";
    private static BufferedWriter fileWriter;
    private static HashMap<Integer, ArrayList> allUsers = new HashMap<>();


    public static void main(String[] args) throws Exception {
        configureOutput();
        final CloseableHttpClient client = HttpClients.createDefault();

        // Start OpenID session
        HttpPost postRequest = new HttpPost("https://data.stackexchange.com/user/authenticate");
        List<NameValuePair> params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("oauth2url", ""));
        params.add(new BasicNameValuePair("openid", "https://openid.stackexchange.com/"));
        postRequest.setEntity(new UrlEncodedFormEntity(params));
        CloseableHttpResponse httpResponse = client.execute(postRequest);
        String loginURL = httpResponse.getFirstHeader("Location").getValue();
        HttpGet getRequest = new HttpGet(loginURL);
        httpResponse = client.execute(getRequest);

        // Login form
        Document document = Jsoup.parse(httpResponse.getEntity().getContent(), "UTF-8", loginURL);
        Element element = document.selectFirst("input[name='fkey']");
        String fkey = element.val();
        element = document.selectFirst("input[name='session']");
        String session = element.val();
        httpResponse.close();

        // Login
        postRequest = new HttpPost("https://openid.stackexchange.com/account/login/submit");
        params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("fkey", fkey));
        params.add(new BasicNameValuePair("session", session));
        params.add(new BasicNameValuePair("email", EMAIL));
        params.add(new BasicNameValuePair("password", PASSWORD));
        postRequest.setEntity(new UrlEncodedFormEntity(params));
        httpResponse = client.execute(postRequest);
        String location = httpResponse.getFirstHeader("Location").getValue();
        httpResponse.close();

        // Resume authentication session
        getRequest = new HttpGet("https://openid.stackexchange.com" + location);
        httpResponse = client.execute(getRequest);
        document = Jsoup.parse(httpResponse.getEntity().getContent(), "UTF-8", "https://data.stackexchange.com");
        element = document.selectFirst("a.my-profile");
        System.out.println("Logged in as " + element.ownText());
        httpResponse.close();

        // Execute query
        int siteID = 1; // 1 = Stack Overflow; see https://data.stackexchange.com/sites
        int queryID = 1210852;
        postRequest = new HttpPost(
                "https://data.stackexchange.com/query/run/" + siteID + "/" + queryID + "/" + 1494017);
        params = new ArrayList<NameValuePair>();
        params.add(new BasicNameValuePair("badgename", "Legendary"));
        postRequest.setEntity(new UrlEncodedFormEntity(params));
        httpResponse = client.execute(postRequest);
        JSONObject jRoot = new JSONObject(IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8"));

        String jobID = jRoot.has("job_id") ? jRoot.getString("job_id") : null;
        if (jobID != null) {
            // Results not in cache yet
            System.out.println("Starting job: " + jobID);
        }
        httpResponse.close();
        while (jRoot.optBoolean("running")) {
            Thread.sleep(1000);
            // Poll job
            getRequest = new HttpGet(
                    "https://data.stackexchange.com/query/job/" + jobID + "?_=" + new Date().getTime());
            httpResponse = client.execute(getRequest);
            jRoot = new JSONObject(IOUtils.toString(httpResponse.getEntity().getContent(), "UTF-8"));
            httpResponse.close();
        }


        // Process results
        if (jRoot.has("resultSets")) {
            String messages = jRoot.getString("messages");
            int totalResults = jRoot.getInt("totalResults"), executionTime = jRoot.getInt("executionTime");
            JSONArray jResultSets = jRoot.getJSONArray("resultSets");
            for (int i = 0; i < jResultSets.length(); i++) {
                JSONObject jResultSet = jResultSets.getJSONObject(i);
                JSONArray jRows = jResultSet.getJSONArray("rows");

                // Get each row from JsonArray
                jRows.forEach(user -> {
                        parseUserObject((JSONArray) user);
                });

                //write to file final result
                fileWriteResult();
            }
        } else {
            String error = jRoot.getString("error");
        }

        System.out.println(jRoot);
        System.out.println(allUsers.toString());

        System.exit(0);
    }

    // create function to write to file
    private static void configureOutput() {
        File fout = new File(FILE_NAME);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fout);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        fileWriter = new BufferedWriter(new OutputStreamWriter(fos));
    }

    // parse each user to compare and remove duplicates
    private static void parseUserObject(JSONArray userJson) {
        ArrayList user = new ArrayList();
        Integer userId = (Integer) userJson.get(0);
        if (allUsers.get(userId) != null) {
            user = allUsers.get(userId);
            Integer answers = (Integer) userJson.get(3) + (Integer) user.get(3);
            Integer questions = (Integer) userJson.get(4) + (Integer) user.get(4);
            String tags = (String) userJson.get(5) + ", " + (String) user.get(5);
            user.set(3, answers);
            user.set(4, questions);
            user.set(5, tags);
        } else {
            for (Object userField : userJson) {
                user.add(userField);
                System.out.print(userField.toString() + " | ");
            }
        }
        allUsers.put(userId, user);
    }


    // Iterate through Map of users to create single output text file
    private static void fileWriteResult() {
        for (Map.Entry mappedUser : allUsers.entrySet()) {
            ArrayList user = (ArrayList) mappedUser.getValue();
            try {
                for (int in = 1; in < user.size(); in++) {
                    fileWriter.write(user.get(in).toString() + " ");
                }
                fileWriter.newLine();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}